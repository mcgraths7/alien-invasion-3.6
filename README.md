In Alien Invasion, the player controls a rocket ship that appears at the
 bottom center of the screen. the player can move the ship right an left
  using the arrow keys and shoot lasers using the space bar. when the game
   begins, a fleet of aliens fills the sky and moves across and down the
    screen. the player shoots and destroys the aliens. if the player shoots
     all of the aliens, a new fleet appears. This fleet moves faster than th
      previous. If any alien hits the player ship or reaches the bottom of
       the screen, the player loses a ship. If the player loses three ships
       , the games ends