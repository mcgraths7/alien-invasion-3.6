import pygame


class Ship:
    """Describes the player ship"""

    def __init__(self, ai_game):
        """Initialize the ship image, and it's starting position on screen"""
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()

        # Tells whether ship is moving left or right
        self.is_moving_right = False
        self.is_moving_left = False

        # Load the ship image and get it's rectangle:
        self.image = pygame.image.load('assets/ship_small.bmp')
        self.rect = self.image.get_rect()

        # Start each ship at the bottom / middle of the screen
        self.rect.midbottom = self.screen_rect.midbottom

    def update_ship_position(self, direction):
        """Update position of ship on screen based on the movement flag"""
        if self.is_moving_right:
            self.rect.x += 1
        elif self.is_moving_left:
            self.rect.x -= 1

    def blit_me(self):
        """Render the ship on screen at it's current position"""
        self.screen.blit(self.image, self.rect)
