import sys

import pygame
from settings import Settings
from ship import Ship


class AlienInvasion:
    """Overall class to manage game assets and behavior"""

    def __init__(self):
        """Initialize the game, and create game resources"""
        pygame.init()
        self.settings = Settings()

        self.screen = pygame.display.set_mode((self.settings.screen_width,
                                              self.settings.screen_height))

        self.ship = Ship(self)

        pygame.display.set_caption("Alien Invasion!")
        self.bg_color = (230, 230, 230)

    def _update_screen(self):
        # Change background color
        self.screen.fill(self.settings.bg_color)

        # Render the ship on screen
        self.ship.blit_me()

        # Make the most recently drawn screen visible
        pygame.display.flip()

    def _check_events(self):
        # Watch for keyboard events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    self.ship.is_moving_right = True
                elif event.key == pygame.K_LEFT:
                    self.ship.is_moving_left = True
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    self.ship.is_moving_right = False
                elif event.key == pygame.K_LEFT:
                    self.ship.is_moving_left = False

    def run_game(self):
        """Start the main game loop"""
        while True:
            self._check_events()
            self.ship.update_ship_position()
            self._update_screen()


if __name__ == '__main__':
    # Make a game instance, then run the game
    ai = AlienInvasion()
    ai.run_game()


# TODO: Continue on page 241
